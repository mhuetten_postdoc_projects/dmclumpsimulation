# CLUMPY 2015.06_corr1_devel            [unit]       [val]

#---------------------------------------------------------------------------
# Cosmological parameters
#---------------------------------------------------------------------------
gCOSMO_HUBBLE                           [-]           0.674
gCOSMO_OMEGA0_M                         [-]           0.315
gCOSMO_DELTA0                           [-]           200
gCOSMO_FLAG_DELTA_REF                   [-]           kRHO_CRIT

#---------------------------------------------------------------------------
# Dark Matter global parameters 
# N.B.: gDM_FRACCMIN_INTEGR and gDM_FRACCMAX_INTEGR unused if gDM_FLAG_CVIR_DIST=kDIRAC
#---------------------------------------------------------------------------
gDM_LOGCDELTA_STDDEV                    [-]           0.14
gDM_SUBS_MMIN                           [Msol]        1e-6  
gDM_SUBS_MMAXFRAC                       [-]           0.01
gDM_RHOSAT                              [Msol/kpc3]   1.e19
gDM_SUBS_NUMBEROFLEVELS                 [-]           1

#---------------------------------------------------------------------------
# Sub-clump properties (universal) for CLUMPY-defined types of haloes,
# i.e. DSPH, GALAXY (but not our Galaxy!), and CLUSTER.
# (N.B.: use gHALO_SUBS_DPDV_RSCALE_TO_RS_HOST=kHOST and/or gHALO_SUBS_DPDV_FLAG_PROFILE=kHOST
#  for the subclump distribution to follow that of the host smooth halo)
#---------------------------------------------------------------------------
gCLUSTER_SUBS_FLAG_CDELTAMDELTA         [-]           kSANCHEZ14_200
gCLUSTER_SUBS_FLAG_PROFILE              [-]           kZHAO
gCLUSTER_SUBS_SHAPE_PARAMS_0            [-]           1.
gCLUSTER_SUBS_SHAPE_PARAMS_1            [-]           3.
gCLUSTER_SUBS_SHAPE_PARAMS_2            [-]           1.
gCLUSTER_SUBS_DPDM_SLOPE                [-]           1.9
gCLUSTER_SUBS_DPDV_FLAG_PROFILE         [-]           kHOST
gCLUSTER_SUBS_DPDV_RSCALE_TO_RS_HOST    [kpc]         kHOST
gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_0       [-]           1.
gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_1       [-]           3.
gCLUSTER_SUBS_DPDV_SHAPE_PARAMS_2       [-]           1.
gCLUSTER_SUBS_MASSFRACTION              [-]           0.1

gDSPH_SUBS_FLAG_CDELTAMDELTA            [-]           kSANCHEZ14_200
gDSPH_SUBS_FLAG_PROFILE                 [-]           kEINASTO
gDSPH_SUBS_SHAPE_PARAMS_0               [-]           0.17
gDSPH_SUBS_SHAPE_PARAMS_1               [-]           3.
gDSPH_SUBS_SHAPE_PARAMS_2               [-]           1.
gDSPH_SUBS_DPDM_SLOPE                   [-]           1.9
gDSPH_SUBS_DPDV_FLAG_PROFILE            [-]           kHOST
gDSPH_SUBS_DPDV_RSCALE_TO_RS_HOST       [kpc]         kHOST
gDSPH_SUBS_DPDV_SHAPE_PARAMS_0          [-]           0.68
gDSPH_SUBS_DPDV_SHAPE_PARAMS_1          [-]           3.
gDSPH_SUBS_DPDV_SHAPE_PARAMS_2          [-]           1.
gDSPH_SUBS_MASSFRACTION                 [-]           0.

gGALAXY_SUBS_FLAG_CDELTAMDELTA          [-]           kSANCHEZ14_200
gGALAXY_SUBS_FLAG_PROFILE               [-]           kZHAO
gGALAXY_SUBS_SHAPE_PARAMS_0             [-]           1.
gGALAXY_SUBS_SHAPE_PARAMS_1             [-]           3.
gGALAXY_SUBS_SHAPE_PARAMS_2             [-]           1.
gGALAXY_SUBS_DPDM_SLOPE                 [-]           1.9
gGALAXY_SUBS_DPDV_FLAG_PROFILE          [-]           kHOST
gGALAXY_SUBS_DPDV_RSCALE_TO_RS_HOST     [kpc]         kHOST
gGALAXY_SUBS_DPDV_SHAPE_PARAMS_0        [-]           1.
gGALAXY_SUBS_DPDV_SHAPE_PARAMS_1        [-]           3.
gGALAXY_SUBS_DPDV_SHAPE_PARAMS_2        [-]           1.
gGALAXY_SUBS_MASSFRACTION               [-]           0.1


#---------------------------------------------------------------------------
# Milky-Way DM total and clump parameters (see profiles.h and clumps.h)
#---------------------------------------------------------------------------
gMW_SUBS_FLAG_CDELTAMDELTA              [-]           kMOLINE17_200
gMW_SUBS_FLAG_PROFILE                   [-]           kZHAO
gMW_SUBS_SHAPE_PARAMS_0                 [-]           1
gMW_SUBS_SHAPE_PARAMS_1                 [-]           3
gMW_SUBS_SHAPE_PARAMS_2                 [-]           1
gMW_SUBS_DPDM_SLOPE                     [-]           1.9
gMW_SUBS_DPDV_FLAG_PROFILE              [-]           kDPDV_SIGMOID_EINASTO
gMW_SUBS_DPDV_RSCALE_TO_RS_HOST         [kpc]         6.531
gMW_SUBS_DPDV_SHAPE_PARAMS_0            [-]           0.68
gMW_SUBS_DPDV_SHAPE_PARAMS_1            [-]           29.2
gMW_SUBS_DPDV_SHAPE_PARAMS_2            [-]           4.24
gMW_SUBS_M1                             [Msol]        1.e8
gMW_SUBS_M2                             [Msol]        1.e10
gMW_SUBS_N_INM1M2                       [-]           90

gMW_RHOSOL                              [GeV/cm3]     0.383465
gMW_RSOL                                [kpc]         8.21
gMW_RMAX                                [kpc]         231.746

gMW_TOT_FLAG_PROFILE                    [-]           kZHAO
gMW_TOT_RSCALE                          [kpc]         19.6
gMW_TOT_SHAPE_PARAMS_0                  [-]           1
gMW_TOT_SHAPE_PARAMS_1                  [-]           3
gMW_TOT_SHAPE_PARAMS_2                  [-]           1

gMW_TRIAXIAL_AXES_0                     [-]           1.47
gMW_TRIAXIAL_AXES_1                     [-]           1.22
gMW_TRIAXIAL_AXES_2                     [-]           0.98
gMW_TRIAXIAL_IS                         [-]           0
gMW_TRIAXIAL_ROTANGLES_0                [deg]         30.
gMW_TRIAXIAL_ROTANGLES_1                [deg]         40.
gMW_TRIAXIAL_ROTANGLES_2                [deg]         20.

gMW_SUBS_TABULATED_IS                   [-]           0
gMW_SUBS_TABULATED_CMIN_OF_R            [-]           /afs/ipp-garching.mpg.de/home/m/mhuetten/software/CLUMPY/data/subhalos_Stref17/cmin_core_eps1.dat
gMW_SUBS_TABULATED_LCRIT                [-]           /afs/ipp-garching.mpg.de/home/m/mhuetten/software/CLUMPY/data/subhalos_Stref17/l_crit_core_eps1.dat
gMW_SUBS_TABULATED_RTIDAL_TO_RS         [-]           /afs/ipp-garching.mpg.de/home/m/mhuetten/software/CLUMPY/data/subhalos_Stref17/rt_over_rs_core.dat

#---------------------------------------------------------------------------
# Particle physics parameters
# N.B.: we recommand the use of kCIRELLI11 for the spectra. You then have to
#    select branching ratios in gPP_BR as a comma-separated
#    list (no space) of Br values for the 28 channels below:
#   0: eL+eL-       1: eR+eR-       2: e+e-
#   3: muL+muL-     4: muR+muR-     5: mu+mu-
#   6: tauL+tauL-   7: tauR+tauR-   8: tau+tau-
#   9: qqbar        10: ccbar       11: bbbar      12: ttbar
#  13: WL+WL        14: WT+WT-      15: W+W-
#  16: ZLZL         17: ZTZT        18: ZZ
#  19: gluglu       20: gammagamma  21: hh
#  22: nuenue       23: numunumu    24: nutaunutau
#  25: VV4e         26: VV4mu       27: VV4tau
#---------------------------------------------------------------------------
gPP_BR                                  [-]           0,0,0,0,0,0,0,0,0,0,0,0.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
gPP_DM_ANNIHIL_DELTA                    [-]           2
gPP_DM_ANNIHIL_SIGMAV_CM3PERS           [cm^3/s]      3.e-24
gPP_DM_DECAY_LIFETIME_S                 [s]           1.e27
gPP_DM_IS_ANNIHIL_OR_DECAY              [-]           1
gPP_DM_MASS_GEV                         [GeV]         500.
gPP_FLAG_SPECTRUMMODEL                  [-]           kCIRELLI11_EW
gPP_NUMIXING_THETA12_DEG                [deg]         34.
gPP_NUMIXING_THETA13_DEG                [deg]          9.
gPP_NUMIXING_THETA23_DEG                [deg]         41.


#---------------------------------------------------------------------------
# Simulation parameters
#  Some parameters can be set to default values respectively automatically
#  adapted by setting to -1 (see documentation).
#  gSIM_OUTPUT_DIR: when set to -1, set to folder from where clumpy is executed. 
#  gSIM_EPS: relative precision seeked for all calculations
#  gSIM_SEED: if=0, seed is chosen from computer clock (for drawing clumps)
#  gSIM_HEALPIX_RING_WEIGHTS_DIR: ring weights dir. for improved quadrature (optional)
#---------------------------------------------------------------------------
gSIM_HEALPIX_NSIDE                      [-]           -1
gSIM_ALPHAINT_DEG                       [deg]         0.5
gSIM_GAUSSBEAM_GAMMA_FWHM_DEG           [deg]         -1
gSIM_GAUSSBEAM_NEUTRINO_FWHM_DEG        [deg]         -1
gSIM_HEALPIX_RING_WEIGHTS_DIR           [-]           /afs/ifh.de/group/cta/scratch/mhuetten/Programs/Healpix_3.20/data

gLIST_HALOES                            [-]           list_generic.txt
gLIST_HALOES_JEANS                      [-]           /afs/ifh.de/group/cta/scratch/mhuetten/Programs/clumpy_dev/data/list_generic_jeans.txt

gSIM_OUTPUT_DIR                         [-]           -1
gSIM_FLUX_AT_E_GEV                      [-]           100.
gSIM_FLUX_EMIN_GEV                      [-]           49.
gSIM_FLUX_EMAX_GEV                      [-]           280.
gSIM_IS_ASTRO_OR_PP_UNITS               [-]           0
gSIM_IS_WRITE_FLUXMAPS                  [-]           0
gSIM_FLUX_IS_INTEG_OR_DIFF              [-]           1
gSIM_IS_WRITE_GALPOWERSPECTRUM          [-]           0
gSIM_IS_WRITE_ROOTFILES                 [-]           0
gSIM_EPS                                [-]           1.e-2
gSIM_FLUX_FLAG_NUFLAVOUR                [-]           kNUMU
gSIM_SEED                               [-]           0
gSTAT_N_REALIZATIONS                    [-]           0

gSIM_USER_RSE                           [-]           0.11
gSIM_PSI_OBS_DEG                        [deg]         0.0
gSIM_THETA_OBS_DEG                      [deg]         0
gSIM_THETA_ORTH_SIZE_DEG                [deg]         d
gSIM_THETA_SIZE_DEG                     [deg]         360

