#### Input Parameter file for clumpy simulations: #############################
#                                                                             #
# Do not change the line numbers of all lines with values and without a (*),  #
# because the values on these lines will be read in as input parameters.      #
#                                                                             #
####                                                                       ####

(08) runname:
	stref_eps1_nfw_rvir_annihil_rse5e-3
	
(11) output directory:
/ptmp/mpp/projects/magic/mhuetten/data/clumpy_sims/2019_Galaxies


###############################################################################
####################           CLUMPY parameters:          ####################

(18) grid resolution Nside:
	-1
		
(21) user_rse:
	0.0002
	
(24) seed:
	0


#####################       Geometrical parameters:	   	  #####################	

(30) Psi position of line of sight (LOS) psiZeroDeg (in degree, galact. coord.):
	0.0

(33) Theta position of LOS thetaZeroDeg (in degree, galact. coord., [-90,90]):
	0

(36) Psi full width of skymap around LOS position psiWidthDeg (in degree):
	d

(39) Theta full width of skymap around LOS position thetaWidthDeg (in degree):
        360

(42) integration angle alphaInt (in degree):
	179.89
	
(45) Gamma smoothing beam width (in degree):
	-1


#####################        Physics parameters:	   	  #####################

(51) Dark Matter annihilation (1) or decay (0):
        1

#### Galactic Halo: ####  Motivation for chosen parameters by Fornasa et al. (2013)
	
(56) Shape of the Milky Way's (MW) total DM density profile (kZHAO or kEINASTO):
	kZHAO
(58) Parameters alpha, beta, gamma (kZHAO), or only alpha (kEINASTO) for the 
(59) MW's total DM density profile:
	1
	3
	1
(63) The total DM density profile is motivated by fits to numerical simulations.

(65) Scaling parameter r_s for the MW's total DM density profile (in kpc),
(66) depends of the chosen profile and measured M(r<r_0) data:
	19.6

(69) Dark Matter saturation density at the galaxy's center:
	1.e19
	
(72) Local DM density rho_sol in GeV/cm^3 (from Nesti/Salucci 2012):
	0.383465
	
(75) Sun's distance from the GC (in kpc):	
	8.21
	
(78) Virial radius of the MW in kpc (not so important?):
	231.746


#### Clumps distribution: ####

(84) Shape of the clumps' distribution function dP/dV (kZHAO or kEINASTO):
        kHOST
(86) Parameters alpha, beta, gamma (kZHAO), or only alpha (kEINASTO) for the
(87) clumps' distribution function:
        kHOST
        kHOST
        kHOST

(92) Scaling parameter for clumps distribution dP/dV profile (relative to r_host):
        1


### Clumps inner profile: ####
	
(98) Shape of the Clumps DM density profile (kZHAO or kEINASTO):
	kZHAO
(100) Parameters alpha, beta, gamma (kZHAO), or only alpha (kEINASTO) for the 
(101) clumps DM density profile:
	1
	3
	1	
	
(106) Model, of how to get the clumps' size (r_vir), r_s and rho_s out of the 
(107) clumps' mass and chosen density profile (kB01_VIR or kENS01_VIR or kSANCHEZ14_200):
	kSANCHEZ14_200

	
### Clumps mass distribution properties:

(113) Slope Power-law mass function dP/dM of subclumping:
	1.9

(116) Number of clumps between M_min and M_max (from simulation):
	276
	
(119) Minimal mass of Dark Matter clumps (in M_sun):
	1e-6

(122) fraction of heaviest clump to host halo total mass:
	0.01
	
(125) M_min for clump mass distribution normalisation (in M_sun), see also line 110
	1.e8
	
(128) M_max for clump mass distribution normalisation (in M_sun), see also line 110
	1.e10
	
### Objects from list?

(133) Draw external objects (like dSph's) from list (put 1 for yes):
	0
	
(136) Name of object list 
(137) (searched for in clumpy folder /afs/ifh.de/group/cta/scratch/mhuetten/Programs/clumpy/data/):
	list_generic.txt


### Others?

(143) How many statistic repetitions?
	0
	
(146) Write power spectrum?
	0
	
(149) scattering of cvir?
	0.14

(152) number of substructure levels?
	1
	
(155) Triaxiality enabled?
	0
		
(158) power spectra pipeline (delete everything but the powerspectra)
    0
        
(161) clumps statistics pipeline (delete everything but the *drawn files)
    1

# Stref model

(166) use tabulated subhalo properties?
    1

(169) File for minimal c(r_gal) after tidal effects
    $CLUMPY/data/subhalos_Stref17/cmin_nfw_eps1.dat

(172) File for l_crit (for evolved sub properties)
    $CLUMPY/data/subhalos_Stref17/lcrit_tables/l_crit_nfw_eps1_rse5e-3.dat

(175) File for r_tidal/r_s (for evolved subs)
    $CLUMPY/data/subhalos_Stref17/rt_over_rs_nfw.dat

###############################################################################


