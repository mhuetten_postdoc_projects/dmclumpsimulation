#!/bin/bash -l


source $HOME/.bash_profile

#
# script to perform Dark Matter clumpy simulations
#
# Author: Moritz Huetten
#
#####################################
# parameters export by parent script

export PARAMETER=RRRRR
export RUNDIR=RUUUN
export QLOG=PEEED
export ODIR=OODIR
export SCRIPTDIR=DIIIR
export DATE=DAATE

# Geometrical parameters:
export psiZeroDeg=AAAAA
export thetaZeroDegGal=BBBBB
export psiWidthDeg=CCCCC
export thetaWidthDeg=DDDDD
export alphaIntDeg=EEEEE
export beamFWHMDeg=ZNNNN

# CLUMPY technical parameters:
export user_rse=FFFFF
export seed=ZHHHH
export nside=ZMMMM

# Galactic Halo:
export gGAL_TOT_FLAG_PROFILE=HHHHH	
export gGAL_TOT_SHAPE_PARAMS_0=IIIII
export gGAL_TOT_SHAPE_PARAMS_1=JJJJJ
export gGAL_TOT_SHAPE_PARAMS_2=KKKKK
export gGAL_TOT_RSCALE=LLLLL
export gGAL_RHOSOL=MMMMM
export gGAL_RSOL=NNNNN
export gGAL_RVIR=OOOOO

# Clumps distribution:
export gGAL_DPDV_FLAG_PROFILE=PPPPP
export gGAL_DPDV_SHAPE_PARAMS_0=QQQQQ
export gGAL_DPDV_SHAPE_PARAMS_1=SSSSS
export gGAL_DPDV_SHAPE_PARAMS_2=TTTTT
export gGAL_DPDV_RSCALE=UUUUU
export gGAL_DPDM_SLOPE=VVVVV
export gDM_MMIN_SUBS=WWWWW
export gGAL_SUBS_N_INM1M2=XXXXX

# Clumps inner profile:
export gGAL_CLUMPS_FLAG_PROFILE=YYYYY
export gGAL_CLUMPS_SHAPE_PARAMS_0=ZZZZZ
export gGAL_CLUMPS_SHAPE_PARAMS_1=ZAAAA
export gGAL_CLUMPS_SHAPE_PARAMS_2=ZBBBB
export gGAL_CLUMPS_FLAG_CVIRMVIR=ZCCCC
export gDM_LOGCVIR_STDDEV=ZQQQQ

# Additional clumpy parameters:
export gDM_MMAXFRAC_SUBS=ZDDDD
export gDM_RHOSAT=ZEEEE
export gGAL_SUBS_M1=ZFFFF
export gGAL_SUBS_M2=ZGGGG

# Particle physics parameters:
export gSIMU_IS_ANNIHIL_OR_DECAY=GGGGG

# Halo list:
export gLIST_HALOES=ZIIII
export gLIST_Bool=ZJJJJ

# technical:
export realisationnumber=$SLURM_ARRAY_TASK_ID
export onlyOneRunBool=ZLLLL
export gSIMU_N_STATISTIC_REPETITIONS=ZOOOO
export gSIMU_IS_GALPOWERSPECTRUM=ZPPPP
export gSIMU_SUBS_NUMBEROFLEVELS=ZRRRR
export gGAL_TRIAXIAL_IS=ZSSSS

export powerspectrapipeline=ZTTTT
export clumpsstatisticspipeline=ZUUUU
export integrationstudy=ZVVVV

# Stref:
export gMW_SUBS_TABULATED_IS=ZZAAA
export gMW_SUBS_TABULATED_CMIN_OF_R=ZZBBB
export gMW_SUBS_TABULATED_LCRIT=ZZCCC
export gMW_SUBS_TABULATED_RTIDAL_TO_RS=ZZDDD


echo
echo " **************************************"
echo " This is a clumpy simulation"
echo " **************************************"
echo 
echo " *** INPUT PARAMETERS: ***"
echo 
echo -n " Psi position of line of sight: " $psiZeroDeg "degrees"
echo -n " Theta position of line of sight: " $thetaZeroDeg "degrees"
echo -n " Full width of skymap around line of sight in psi direction: " $psiWidthDeg "degrees"
echo -n " Full width of skymap around line of sight in theta direction: " $thetaWidthDeg "degrees"
echo -n " grid resolution: " $nside 
echo -n " integration angle: " $alphaIntDeg "degrees"
echo -n " user_rse: " $user_rse 
echo
echo " *** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS SEE dmClumps.sub.sh ***"
echo 

###############################################################################

###############################################################################
# explanation of different directories:
# RUNDIR:  parent directory of run, the one export in line (08) of
#		   anisotropySimulation.parameters file, e.g. 14-07-13-CrabTest
# ODIR:	   parent directory of specific runparameter variation 
# WORKDIR: directory within ODIR for the number of identical realizations
# if no runparameter file: ODIR= RUNDIR
# if realizationNumber = 1: WORKDIR = ODIR

###############################################################################
# clean up output directory and create subfolders for each run:

# in case of multiple runs with runparameter: RUNDIR: parent directory, ODIR: directory of specific runparameter

# make temporary directory for each realisation:
if  [[ $onlyOneRunBool == "0" ]]; then
	export WORKDIR=$ODIR/realisation-${realisationnumber}
	echo "WORKDIR: " $WORKDIR
	rm -r $WORKDIR/*
	mkdir -p $WORKDIR/Inputfiles
	cd $WORKDIR
else
	export WORKDIR=$ODIR # = current directory"$(pwd)"
	cd $WORKDIR
fi

if [[ $powerspectrapipeline == "1" ]]; then
	export MODE="p" # MODE="n" prevents any file to be written
	export ISWRITEROOTFILES="0"
else
	export MODE="p"
	export ISWRITEROOTFILES="0"
fi

if [[ $clumpsstatisticspipeline == "1" ]]; then
	export ISWRITEROOTFILES="0"
fi

###############################################################################
# modify clumpy parameter file:
echo " *** modify clumpy parameter file: ***"
sed  's|EEEEZ|'$alphaIntDeg'|' $SCRIPTDIR/clumpyV3.clumpy_params.txt > $WORKDIR/Inputfiles/tmp1.txt
sed  's|GGGGZ|'$gSIMU_IS_ANNIHIL_OR_DECAY'|' $WORKDIR/Inputfiles/tmp1.txt > $WORKDIR/Inputfiles/tmp2.txt
sed  's|HHHHZ|'$gGAL_TOT_FLAG_PROFILE'|' $WORKDIR/Inputfiles/tmp2.txt > $WORKDIR/Inputfiles/tmp3.txt
sed  's|IIIIZ|'$gGAL_TOT_SHAPE_PARAMS_0'|' $WORKDIR/Inputfiles/tmp3.txt > $WORKDIR/Inputfiles/tmp4.txt
sed  's|JJJJZ|'$gGAL_TOT_SHAPE_PARAMS_1'|' $WORKDIR/Inputfiles/tmp4.txt > $WORKDIR/Inputfiles/tmp5.txt
sed  's|KKKKZ|'$gGAL_TOT_SHAPE_PARAMS_2'|' $WORKDIR/Inputfiles/tmp5.txt > $WORKDIR/Inputfiles/tmp6.txt
sed  's|LLLLZ|'$gGAL_TOT_RSCALE'|' $WORKDIR/Inputfiles/tmp6.txt > $WORKDIR/Inputfiles/tmp7.txt
sed  's|MMMMZ|'$gGAL_RHOSOL'|' $WORKDIR/Inputfiles/tmp7.txt > $WORKDIR/Inputfiles/tmp8.txt
sed  's|NNNNZ|'$gGAL_RSOL'|' $WORKDIR/Inputfiles/tmp8.txt > $WORKDIR/Inputfiles/tmp9.txt
sed  's|OOOOZ|'$gGAL_RVIR'|' $WORKDIR/Inputfiles/tmp9.txt > $WORKDIR/Inputfiles/tmp10.txt
sed  's|PPPPZ|'$gGAL_DPDV_FLAG_PROFILE'|' $WORKDIR/Inputfiles/tmp10.txt > $WORKDIR/Inputfiles/tmp11.txt
sed  's|QQQQZ|'$gGAL_DPDV_SHAPE_PARAMS_0'|' $WORKDIR/Inputfiles/tmp11.txt > $WORKDIR/Inputfiles/tmp12.txt
sed  's|SSSSZ|'$gGAL_DPDV_SHAPE_PARAMS_1'|' $WORKDIR/Inputfiles/tmp12.txt > $WORKDIR/Inputfiles/tmp13.txt
sed  's|TTTTZ|'$gGAL_DPDV_SHAPE_PARAMS_2'|' $WORKDIR/Inputfiles/tmp13.txt > $WORKDIR/Inputfiles/tmp14.txt
sed  's|UUUUZ|'$gGAL_DPDV_RSCALE'|' $WORKDIR/Inputfiles/tmp14.txt > $WORKDIR/Inputfiles/tmp15.txt
sed  's|VVVVZ|'$gGAL_DPDM_SLOPE'|' $WORKDIR/Inputfiles/tmp15.txt > $WORKDIR/Inputfiles/tmp16.txt
sed  's|WWWWZ|'$gDM_MMIN_SUBS'|' $WORKDIR/Inputfiles/tmp16.txt > $WORKDIR/Inputfiles/tmp17.txt
sed  's|XXXXZ|'$gGAL_SUBS_N_INM1M2'|' $WORKDIR/Inputfiles/tmp17.txt > $WORKDIR/Inputfiles/tmp18.txt
sed  's|YYYYZ|'$gGAL_CLUMPS_FLAG_PROFILE'|' $WORKDIR/Inputfiles/tmp18.txt > $WORKDIR/Inputfiles/tmp19.txt
sed  's|ZZZZX|'$gGAL_CLUMPS_SHAPE_PARAMS_0'|' $WORKDIR/Inputfiles/tmp19.txt > $WORKDIR/Inputfiles/tmp20.txt
sed  's|ZAAAZ|'$gGAL_CLUMPS_SHAPE_PARAMS_1'|' $WORKDIR/Inputfiles/tmp20.txt > $WORKDIR/Inputfiles/tmp21.txt
sed  's|ZBBBZ|'$gGAL_CLUMPS_SHAPE_PARAMS_2'|' $WORKDIR/Inputfiles/tmp21.txt > $WORKDIR/Inputfiles/tmp22.txt
sed  's|ZCCCZ|'$gGAL_CLUMPS_FLAG_CVIRMVIR'|' $WORKDIR/Inputfiles/tmp22.txt > $WORKDIR/Inputfiles/tmp23.txt
sed  's|ZDDDZ|'$gDM_MMAXFRAC_SUBS'|' $WORKDIR/Inputfiles/tmp23.txt > $WORKDIR/Inputfiles/tmp24.txt
sed  's|ZEEEZ|'$gDM_RHOSAT'|' $WORKDIR/Inputfiles/tmp24.txt > $WORKDIR/Inputfiles/tmp25.txt
sed  's|ZFFFZ|'$gGAL_SUBS_M1'|' $WORKDIR/Inputfiles/tmp25.txt > $WORKDIR/Inputfiles/tmp26.txt
sed  's|ZGGGZ|'$gGAL_SUBS_M2'|' $WORKDIR/Inputfiles/tmp26.txt > $WORKDIR/Inputfiles/tmp27.txt
sed  's|ZHHHZ|'$seed'|' $WORKDIR/Inputfiles/tmp27.txt > $WORKDIR/Inputfiles/tmp28.txt
sed  's|ZMMMZ|'$nside'|' $WORKDIR/Inputfiles/tmp28.txt > $WORKDIR/Inputfiles/tmp29.txt
sed  's|ZNNNZ|'$beamFWHMDeg'|' $WORKDIR/Inputfiles/tmp29.txt > $WORKDIR/Inputfiles/tmp30.txt
sed  's|ZIIIZ|'$gLIST_HALOES'|' $WORKDIR/Inputfiles/tmp30.txt > $WORKDIR/Inputfiles/tmp31.txt
sed  's|ZOOOZ|'$gSIMU_N_STATISTIC_REPETITIONS'|' $WORKDIR/Inputfiles/tmp31.txt > $WORKDIR/Inputfiles/tmp32.txt
sed  's|ZPPPZ|'$gSIMU_IS_GALPOWERSPECTRUM'|' $WORKDIR/Inputfiles/tmp32.txt > $WORKDIR/Inputfiles/tmp33.txt
sed  's|ZQQQZ|'$gDM_LOGCVIR_STDDEV'|' $WORKDIR/Inputfiles/tmp33.txt > $WORKDIR/Inputfiles/tmp34.txt
sed  's|ZRRRZ|'$gSIMU_SUBS_NUMBEROFLEVELS'|' $WORKDIR/Inputfiles/tmp34.txt > $WORKDIR/Inputfiles/tmp35.txt
sed  's|ZUUUZ|'$psiZeroDeg'|' $WORKDIR/Inputfiles/tmp35.txt > $WORKDIR/Inputfiles/tmp36.txt
sed  's|ZVVVZ|'$thetaZeroDegGal'|' $WORKDIR/Inputfiles/tmp36.txt > $WORKDIR/Inputfiles/tmp37.txt
sed  's|ZWWWZ|'$psiWidthDeg'|' $WORKDIR/Inputfiles/tmp37.txt > $WORKDIR/Inputfiles/tmp38.txt
sed  's|ZXXXZ|'$thetaWidthDeg'|' $WORKDIR/Inputfiles/tmp38.txt > $WORKDIR/Inputfiles/tmp39.txt
sed  's|ZYYYZ|'$user_rse'|' $WORKDIR/Inputfiles/tmp39.txt > $WORKDIR/Inputfiles/tmp40.txt
sed  's|ZTTTZ|'$ISWRITEROOTFILES'|' $WORKDIR/Inputfiles/tmp40.txt > $WORKDIR/Inputfiles/tmp41.txt
sed  's|ZZAAZ|'$gMW_SUBS_TABULATED_IS'|' $WORKDIR/Inputfiles/tmp41.txt > $WORKDIR/Inputfiles/tmp42.txt
sed  's|ZZBBZ|'$gMW_SUBS_TABULATED_CMIN_OF_R'|' $WORKDIR/Inputfiles/tmp42.txt > $WORKDIR/Inputfiles/tmp43.txt
sed  's|ZZCCZ|'$gMW_SUBS_TABULATED_LCRIT'|' $WORKDIR/Inputfiles/tmp43.txt > $WORKDIR/Inputfiles/tmp44.txt
sed  's|ZZDDZ|'$gMW_SUBS_TABULATED_RTIDAL_TO_RS'|' $WORKDIR/Inputfiles/tmp44.txt > $WORKDIR/Inputfiles/tmp45.txt
sed  's|ZSSSZ|'$gGAL_TRIAXIAL_IS'|' $WORKDIR/Inputfiles/tmp45.txt > $WORKDIR/Inputfiles/clumpyV3.clumpy_params.txt

rm $WORKDIR/Inputfiles/tmp*

# copy clumpy binary into computation directory on cluster:
if [[ $clumpsstatisticspipeline == "1" ]]; then
        cp $CLUMPY/bin/clumpy $WORKDIR
else
        cp /afs/ipp-garching.mpg.de/home/m/mhuetten/software/CLUMPY_unchanged/bin/clumpy $WORKDIR 
fi

chmod u+x $WORKDIR/Inputfiles/clumpyV3.clumpy_params.txt


###############################################################################
# start clumpy routine:
echo " *** start clumpy routine: ***"
if [[ $gLIST_Bool == "1" ]]; then
	$WORKDIR/clumpy -g8 -${MODE} -i $WORKDIR/Inputfiles/clumpyV3.clumpy_params.txt | tee $ODIR/Logfiles/clumpy-realisation-${realisationnumber}.log
        exitcode=$?
else
	$WORKDIR/clumpy -g7 -${MODE} -i $WORKDIR/Inputfiles/clumpyV3.clumpy_params.txt| tee $ODIR/Logfiles/clumpy-realisation-${realisationnumber}.log
        exitcode=$?
fi

echo -n " Clumpy exit code is: " $exitcode

if [[ $exitcode != "0" ]]; then
        echo "Clumpy code aborted with non-zero exit code. Abort."
        rm -f $WORKDIR
        exit 1
fi


if [[ $onlyOneRunBool == "0" ]]; then
	mv Inputfiles/clumpyV3.clumpy_params.txt $ODIR/Inputfiles/clumpyV3.clumpy_params.txt
	mv $WORKDIR/Inputfiles/list-clumps.txt $WORKDIR/list-clumps.txt
	rm -rf Inputfiles
fi

if [[ $powerspectrapipeline == "1" ]]; then
	# for power spectrum only calculation, delete everything else again
	rm -rf Logfiles
	mv *drawn clumps.drawn
	
	mv annihil* tmp1.fits

	$WORKDIR/clumpy -o2 tmp1.fits 2 1
	mv *JFACTOR_PER_SR* tmp2.fits
	
	cp $CLUMPY_FIXED/python_helperscripts/makeFitsImage.py $WORKDIR
	python $WORKDIR/makeFitsImage.py -i tmp2.fits

	mv *image.fits skymap.fits
	rm pop*
	rm tmp*
	rm makeFitsImage.py

	##sleep 20
fi

rm -f $WORKDIR/clumpy
rm -rf $WORKDIR/PPPC4DMID-spectra

if [[ $clumpsstatisticspipeline == "1" ]]; then
	# for power spectrum only calculation, delete everything else again
	if [[ $onlyOneRunBool == "0" ]]; then
		# check if run ended successfully:
		mv *.fits check.fits
		#if ( -f "check.fits" ) then
			if [[ $integrationstudy == "0" ]]; then
				mv *.drawn $ODIR/realisation-${realisationnumber}.drawn
				cd $ODIR
				rm -r $WORKDIR
			else
				mv *.list alphaint_${alphaIntDeg}.list
				rm check.fits
			fi
		#endif
	else
		rm -rf Logfiles
		rm *.fits
	fi

fi

echo "submission script finished!"

##exit
