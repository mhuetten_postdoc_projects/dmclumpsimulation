#!/bin/sh
#
# script run clumpy simulations:
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
	echo
	echo "Submission script for a clumpyV3 simulation:"
	echo
	echo "clumpyV3.sub.sh <clumpyV3.parameters> [number of simulations] [developing mode] [clumpyV2.runparameter]"
	echo
	echo "  for the clumpyV2.parameters file use template without changing line numbers"
	echo "  in case of choosing a varying parameter in a clumpyV2.runparameter file,"
	echo "  write name of parameter in first line, then al values you want to compute."
	echo "  Example:"
	echo
	echo "    user_rse"
	echo "    5.0"
	echo "    10.0"
	echo "    15.0"
	echo
	
	exit
fi

export parameterfile=$PWD/$1

#read variables from parameter file:

# General parameters:
export runname=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
export outdir=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')

# CLUMPY technical parameters:
export nside=$(cat $1 	        | head -n19 | tail -n1 | sed -e 's/^[ \t]*//')
export user_rse=$(cat $1 		| head -n22 | tail -n1 | sed -e 's/^[ \t]*//') 
export gSIMU_SEED=$(cat $1 	    | head -n25 | tail -n1 | sed -e 's/^[ \t]*//')

# Geometrical parameters:
export psiZeroDeg=$(cat $1 		| head -n31 | tail -n1 | sed -e 's/^[ \t]*//') 
export thetaZeroDeg=$(cat $1 	| head -n34 | tail -n1 | sed -e 's/^[ \t]*//')
export psiWidthDeg=$(cat $1 	| head -n37 | tail -n1 | sed -e 's/^[ \t]*//')
export thetaWidthDeg=$(cat $1 	| head -n40 | tail -n1 | sed -e 's/^[ \t]*//')
export alphaIntDeg=$(cat $1 	| head -n43 | tail -n1 | sed -e 's/^[ \t]*//')
export beamFWHMDeg=$(cat $1 	| head -n46 | tail -n1 | sed -e 's/^[ \t]*//')

# Physics parameters:
export gSIMU_IS_ANNIHIL_OR_DECAY=$(cat $1 | head -n52 | tail -n1 | sed -e 's/^[ \t]*//')

# Galactic Halo:
export gGAL_TOT_FLAG_PROFILE=$(cat $1 	| head -n57 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_0=$(cat $1 | head -n60 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_1=$(cat $1 | head -n61 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_2=$(cat $1 | head -n62 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_RSCALE=$(cat $1 		| head -n67 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_RHOSAT=$(cat $1			 	| head -n70 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RHOSOL=$(cat $1 			| head -n73 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RSOL=$(cat $1 				| head -n76 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RVIR=$(cat $1 				| head -n79 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps distribution:
export gGAL_DPDV_FLAG_PROFILE=$(cat $1 	 | head -n85 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_0=$(cat $1 | head -n88 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_1=$(cat $1 | head -n89 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_2=$(cat $1 | head -n90 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_RSCALE=$(cat $1		 | head -n93 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps inner profile:
export gGAL_CLUMPS_FLAG_PROFILE=$(cat $1   | head -n99 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_0=$(cat $1 | head -n102 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_1=$(cat $1 | head -n103 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_2=$(cat $1 | head -n104 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_FLAG_CVIRMVIR=$(cat $1  | head -n108 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps mass distribution properties:
export gGAL_DPDM_SLOPE=$(cat $1			 | head -n114 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_N_INM1M2=$(cat $1		 | head -n117 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMIN_SUBS=$(cat $1			 | head -n120 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMAXFRAC_SUBS=$(cat $1 		 | head -n123 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M1=$(cat $1 			 | head -n126 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M2=$(cat $1 			 | head -n129 | tail -n1 | sed -e 's/^[ \t]*//')

# Draw object from list?
export gLIST_Bool=$(cat $1		| head -n134 | tail -n1 | sed -e 's/^[ \t]*//')
export gLIST_HALOES=$(cat $1	| head -n138 | tail -n1 | sed -e 's/^[ \t]*//')

# Special parameters:
export gSIMU_N_STATISTIC_REPETITIONS=$(cat $1	| head -n144 | tail -n1 | sed -e 's/^[ \t]*//')
export gSIMU_IS_GALPOWERSPECTRUM=$(cat $1		| head -n147 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_LOGCVIR_STDDEV=$(cat $1		        | head -n150 | tail -n1 | sed -e 's/^[ \t]*//')
export gSIMU_SUBS_NUMBEROFLEVELS=$(cat $1		| head -n153 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TRIAXIAL_IS=$(cat $1		        | head -n156 | tail -n1 | sed -e 's/^[ \t]*//')

export powerspectrapipeline=$(cat $1		    | head -n159 | tail -n1 | sed -e 's/^[ \t]*//')
export clumpsstatisticspipeline=$(cat $1	    | head -n162 | tail -n1 | sed -e 's/^[ \t]*//')

export gMW_SUBS_TABULATED_IS=$(cat $1                | head -n167 | tail -n1 | sed -e 's/^[ \t]*//')
export gMW_SUBS_TABULATED_CMIN_OF_R=$(cat $1            | head -n170 | tail -n1 | sed -e 's/^[ \t]*//')
export gMW_SUBS_TABULATED_LCRIT=$(cat $1                | head -n173 | tail -n1 | sed -e 's/^[ \t]*//')
export gMW_SUBS_TABULATED_RTIDAL_TO_RS=$(cat $1            | head -n176 | tail -n1 | sed -e 's/^[ \t]*//')


if [ -n "$4" ]
then
	PLIST=$4
	# number of parameters:
	export runparameter=$(cat $PLIST 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $PLIST | tail -n +2`
	echo -n "Running parameter is: "
	echo $runparameter
	echo
fi

# number of repeated realisations:
samplenumber=1
if [ -n "$2" ]; then
samplenumber=$2
fi

# Scriptdirectory is current directory, save it:
SCRIPTDIR="$(pwd)"

# Save the date:
DATE=`date +"%y%m%d"`

# Make Logfile directory for submission scripts and batch logfiles output:
cd $HOME/Logs/DMCLUMPS-SIMULATION
#mkdir -p $DATE
QLOG=$HOME/Logs/DMCLUMPS-SIMULATION/ #$DATE

BATCHLOG=$QLOG

# developing mode?
devmode=0
if [ -n "$3" ]; then
devmode=$3
fi
if [[ $devmode == "0" ]]; then
	computingTime=96:05:59
        computingTime=11:59:59
	memorySize=8G
        queue="long"
        queue="standard"
elif [[ $devmode == "1" ]]; then
	computingTime=00:29:59
	memorySize=2G
        queue="short"
else
	echo "EXIT: devmode variable must be either 0 or 1"
	echo
	exit
fi		

cd $outdir
mkdir -p $runname
cd $runname
RUNDIR="$(pwd)"
# now forget outdir

# empty directory if start from scratch:
files=($RUNDIR/*)
if [ ${#files[@]} -gt 0 ]; then
    while true; do
    read -p "Do you really want to delete all old files? (yes/no) " yn
    case $yn in
        [yes]* ) break;;
        [no]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
    done
    echo "ok, continue..."
    rm -r *
fi



# skeleton script
FSCRIPT="clumpyV3.qsub"

if [[ $clumpsstatisticspipeline == "1" ]]; then
	mkdir -p $RUNDIR/Batchlogs
	BATCHLOG=$RUNDIR/Batchlogs
fi

integrationstudy=0

###############################################################################
# If NO runparameter file exists:
###############################################################################

if [ ! -n "$4" ]; then
	echo "now submitting single job to batch"
	# RUNDIR is equal ODIR in this case!
	
	AFIL=singlerundummy
	
	# AFIL is not used in this case!

	cd $RUNDIR
	ODIR="$(pwd)"
	#rm -r Logfiles
	#rm -r Inputfiles
		
	mkdir -p Inputfiles
	mkdir -p Logfiles
		
	cd $SCRIPTDIR
	cp clumpyV3.*sh $ODIR/Inputfiles/
        cp $parameterfile $ODIR/Inputfiles/
	rm $ODIR/Inputfiles/clumpyV3.clumpy_params.txt
	rm $ODIR/Inputfiles/clumpyV3.runparameter
		
	startnum=$((1))
	one=$((1))
	endnum=$(($startnum+$samplenumber-$one))

		
	if [[ $samplenumber == "1" ]]; then
		FNAM="$QLOG/clumpyV3-$runname"
		onlyOneRunBool=1
	else 
		FNAM="$QLOG/clumpyV3-$runname-multijob"
		onlyOneRunBool=0
		echo "starting multisample realisation"
	fi
					
	sed -e "s|FFFFF|$user_rse|" \
		-e "s|PEEED|$QLOG|" \
		-e "s|OODIR|$ODIR|" \
		-e "s|DAATE|$DATE|" \
		-e "s|MMMMM|$gGAL_RHOSOL|" \
		-e "s|NNNNN|$gGAL_RSOL|" \
		-e "s|OOOOO|$gGAL_RVIR|" \
		-e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
		-e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
		-e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
		-e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
		-e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
		-e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
		-e "s|WWWWW|$gDM_MMIN_SUBS|" \
		-e "s|XXXXX|$gGAL_SUBS_N_INM1M2|" \
		-e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
		-e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
		-e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
		-e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
		-e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
		-e "s|AAAAA|$psiZeroDeg|" \
		-e "s|BBBBB|$thetaZeroDeg|" \
		-e "s|CCCCC|$psiWidthDeg|" \
		-e "s|DDDDD|$thetaWidthDeg|" \
		-e "s|EEEEE|$alphaIntDeg|" \
		-e "s|ZNNNN|$beamFWHMDeg|" \
		-e "s|ZMMMM|$nside|" \
		-e "s|RRRRR|$AFIL|" \
		-e "s|RUUUN|$RUNDIR|" \
		-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
		-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
		-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
		-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
		-e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
		-e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
		-e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
		-e "s|LLLLL|$gGAL_TOT_RSCALE|" \
		-e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
		-e "s|ZEEEE|$gDM_RHOSAT|" \
		-e "s|ZFFFF|$gGAL_SUBS_M1|" \
		-e "s|ZGGGG|$gGAL_SUBS_M2|" \
		-e "s|ZHHHH|$gSIMU_SEED|" \
		-e "s|ZIIII|$gLIST_HALOES|" \
		-e "s|ZJJJJ|$gLIST_Bool|" \
		-e "s|ZLLLL|$onlyOneRunBool|" \
		-e "s|ZOOOO|$gSIMU_N_STATISTIC_REPETITIONS|" \
		-e "s|ZPPPP|$gSIMU_IS_GALPOWERSPECTRUM|" \
		-e "s|ZQQQQ|$gDM_LOGCVIR_STDDEV|" \
		-e "s|ZRRRR|$gSIMU_SUBS_NUMBEROFLEVELS|" \
		-e "s|ZSSSS|$gGAL_TRIAXIAL_IS|" \
		-e "s|ZTTTT|$powerspectrapipeline|" \
		-e "s|ZUUUU|$clumpsstatisticspipeline|" \
		-e "s|ZVVVV|$integrationstudy|" \
                -e "s|ZZAAA|$gMW_SUBS_TABULATED_IS|" \
                -e "s|ZZBBB|$gMW_SUBS_TABULATED_CMIN_OF_R|" \
                -e "s|ZZCCC|$gMW_SUBS_TABULATED_LCRIT|" \
                -e "s|ZZDDD|$gMW_SUBS_TABULATED_RTIDAL_TO_RS|" \
		-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh

	chmod u+x $FNAM.sh
	echo "script name is:" $FNAM.sh
	
	JOBID=`sbatch -J $runname -A mpp -t $computingTime --mem=$memorySize -o ${runname}_%A_%a.out -e ${runname}_%A_%a.err -D $BATCHLOG --mail-user=mhuetten@mpp.mpg.de --mail-type=FAIL --export=ALL --partition=$queue --array=$startnum-$endnum:1 $FNAM.sh` #-hold_jid 5306638
	JOBID=${JOBID:19:30}
	
        if [[ $devmode == "1" ]]; then
                echo "Submitted job" $JOBID "in developing mode ("$computingTime"," $memorySize "RAM)"
        else
                echo "Submitted job" $JOBID
        fi
fi
exit
